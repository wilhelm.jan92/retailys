# Imports
# ----------------------------------
import wget
import os
from dotenv import load_dotenv
load_dotenv()
import zipfile
import xml.etree.ElementTree as ET

# Setting variables
# ----------------------------------
uri = os.environ.get('uri')
current_dir = os.getcwd()
xml_file = 'export_full.xml'
file = 'retailSys.zip'
filename = f"{current_dir}\{file}"
url=uri

# Functions
# ----------------------------------
def fetch_files(filename):
    # Script checks if file exists
    if not os.path.exists(filename): 
        # Scripts tries to download file
        try:
           return wget.download(uri,out=filename)
        except Exception as e:
            print('Error while downloading file.')
    else:
        pass

def unzip_files(filename,dir):
    # Script extracts XML from zip
    with zipfile.ZipFile(filename, 'r') as zip_ref:
        zip_ref.extractall(dir)


def select_task(task,file):
    # Parsing xml file
    tree = ET.parse(file)
    root = tree.getroot()
    items = []
    # Match case for selecting task
    match task:
        case '1':
            for item in root.findall("items/item"):
                            items += item
            print(f"Celkový počet produktů je: {len(items)}")
        case '2':
            for item in root.findall("items/item"):
                            print(item.attrib['name']) 
        case '3':
            for item in root.findall("items/item"):
                print(item.attrib['name']) 
                print("---------------------") 
                for x in item.findall("parts/part/item"):
                        print(x.attrib['name'])
                print("---------------------") 
                  
        case _:        
            return 0  

# Functions call
# ----------------------------------
zip = fetch_files(file)
unzip_files(filename,current_dir)
task = input("Zadej číslo úkolu (1,2,3): ")
select_task(task,xml_file)










